/* Define R,G,B pwm pin */
#define RED_PIN   3
#define GREEN_PIN 5
#define BLUE_PIN  6

/* Color selection enum */
enum
{
  RED = 0,
  GREEN,
  BLUE
};
#define DELAY_DURATION 33
int fade_val;

int SOUND_VOLUME_PIN = A0; /* Input pin for the potentiometer */
int SOUND_THRESHOLD = 550; /* Sound level threshold */
int SOUND_MAX = 600; /* Sound maximum level */
int soundValue;

int ledMaxValue;
int randomMaxColor;

void setup()
{
  /* Initialize pin mode */
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);
  analogWrite(RED_PIN, 0);
  analogWrite(GREEN_PIN, 0);
  analogWrite(BLUE_PIN, 0);
  
  /* if analog input pin 0 is unconnected, random analog noise will cause the call to randomSeed() to generate
     different seed numbers each time the sketch runs. randomSeed() will then shuffle the random function. */
  randomSeed(analogRead(RED_PIN));
  randomSeed(analogRead(GREEN_PIN));
  randomSeed(analogRead(BLUE_PIN));
  
  /* Initializing.....7 color testing.... */
  OneColor(RED_PIN, GREEN_PIN, BLUE_PIN);  //Red testing
  OneColor(GREEN_PIN, RED_PIN, BLUE_PIN);  //Green testing
  OneColor(BLUE_PIN, RED_PIN, GREEN_PIN);  //Blue testing
  TwoColor(RED_PIN, GREEN_PIN, BLUE_PIN);  //Yellow testing
  TwoColor(RED_PIN, BLUE_PIN, GREEN_PIN);  //Magenta testing
  TwoColor(GREEN_PIN, BLUE_PIN, RED_PIN);  //Cyan testing
  ThreeColor(RED_PIN, GREEN_PIN, BLUE_PIN);  //White testing
  
  Serial.begin(9600);
}

void loop()
{ 
  soundValue = analogRead(SOUND_VOLUME_PIN);
  if (soundValue > SOUND_THRESHOLD)
  {
    Serial.print("Sound Sensor Volume = ");
    Serial.println(soundValue);

    /* Sound level between 500~600, mapping it to analog value 0~255 */
    ledMaxValue = map(soundValue, SOUND_THRESHOLD, SOUND_MAX, 0, 255);
    /* For showing different color from LED, we pick a color (R, G or B) as base one. */
    randomMaxColor = random(0, 3);

    /* Show re-mapped PWM value and what base color is.*/
    Serial.print("  PWM = ");
    Serial.print(ledMaxValue);
    Serial.print(" , Max Color = ");
    switch(randomMaxColor)
    {
      /* For example, if re-mapped PWM value is 220 and base color is RED.
         Write value 220 to RED_PIN, then get the random value between 0~220 and 
         set it to GREEN_PIN and BLUE_PIN.
         Maybe GREEN value is 20, BLUE is 60. LED will flash "Crimson" color. */         
      case RED:
        Serial.println("Red");
        analogWrite(RED_PIN, ledMaxValue);
        analogWrite(GREEN_PIN, random(0, ledMaxValue));
        analogWrite(BLUE_PIN, random(0, ledMaxValue));
        break;
        
      case GREEN:
        Serial.println("Green");
        analogWrite(GREEN_PIN, ledMaxValue);
        analogWrite(RED_PIN, random(0, ledMaxValue));
        analogWrite(BLUE_PIN, random(0, ledMaxValue));
        break;
        
      case BLUE:
        Serial.println("Blue");
        analogWrite(BLUE_PIN, ledMaxValue);
        analogWrite(RED_PIN, random(0, ledMaxValue));
        analogWrite(GREEN_PIN, random(0, ledMaxValue));
        break;
        
      default:
        Serial.println("White");
        analogWrite(RED_PIN, ledMaxValue);
        analogWrite(GREEN_PIN, ledMaxValue);
        analogWrite(BLUE_PIN, ledMaxValue);
        break;
    }
    delay(DELAY_DURATION); /* no delay, LED will flash quickly */
  }
  else
  {    
    analogWrite(RED_PIN, 0);
    analogWrite(GREEN_PIN, 0);
    analogWrite(BLUE_PIN, 0);
  }
}

void OneColor(int _on, int _off1, int _off2)
{
  analogWrite(_on, 0);
  analogWrite(_off1, 0);
  analogWrite(_off2, 0);
  delay(300);
  
  for (fade_val=0; fade_val<256; fade_val=fade_val+5)
  {
    analogWrite(_on, fade_val);
    delay(DELAY_DURATION);
  }
}

void TwoColor(int _on1, int _on2, int _off)
{
  analogWrite(_on1, 0);
  analogWrite(_on2, 0);
  analogWrite(_off, 0);
  delay(300);
  
  for (fade_val=0; fade_val<256; fade_val=fade_val+5)
  {
    analogWrite(_on1, fade_val);
    analogWrite(_on2, fade_val);
    delay(DELAY_DURATION);
  }
}

void ThreeColor(int _on1, int _on2, int _on3)
{
  analogWrite(_on1, 0);
  analogWrite(_on2, 0);
  analogWrite(_on3, 0);
  for (fade_val=0; fade_val<256; fade_val=fade_val+5)
  {
    analogWrite(_on1, fade_val);
    analogWrite(_on2, fade_val);
    analogWrite(_on3, fade_val);
    delay(DELAY_DURATION);
  }
}
